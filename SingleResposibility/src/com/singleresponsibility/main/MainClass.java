package com.singleresponsibility.main;

import com.singleresp.exam.Exam;
import com.singleresp.showresult.ShowResult;

public class MainClass {

	public static void main(String[] args) {
		Exam e = new Exam();
		e.setSubject("physic");
		e.setTechear("Inshal");
		
		
		ShowResult sr = new ShowResult();
		sr.showResult(e.getSubject());
		sr.showResult(e.getTechear());
		

	}

}
