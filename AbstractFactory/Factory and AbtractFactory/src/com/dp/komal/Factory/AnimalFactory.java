package com.dp.komal.Factory;

public class AnimalFactory {
	public WildAnimals getAnimal(String animal)
	{
		if(animal == null)
		return null;
		if(animal.equalsIgnoreCase("lion")){
			return new Lion();}
		else if(animal.equalsIgnoreCase("Tiger"))
		{	return new Tiger();}
		return null;
	}
}
