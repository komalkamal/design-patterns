package com.dp.komal.Factory;

public class Main {
	public static void main(String[] args){

	AnimalFactory factory = new AnimalFactory();
	
	WildAnimals wildanimals;
	
	wildanimals = factory.getAnimal("Lion");
	wildanimals.eat();
	
	wildanimals = factory.getAnimal("Tiger");
	wildanimals.eat();
	}
}