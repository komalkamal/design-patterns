package com.dp.komal.abstractFactory;

public class WildAnimalsFactory  implements AbstractFactory{

	@Override
	public WildAnimals getwildanimal(String animalsType) {
		if(animalsType == null)
		return null;
		 if(animalsType.equalsIgnoreCase("Lion")){
	            return new Lion();
	        }else if(animalsType.equalsIgnoreCase("Tiger")){
	            return new Tiger();
	        }
	        return null;
	}

	@Override
	public PetAnimals getpetanimal(String animalsType) {
		
		return null;
	}

}
