package com.dp.komal.abstractFactory;

public class PetAnimalsFactory implements AbstractFactory {

	@Override
	public WildAnimals getwildanimal(String animalsType) {
		
		return null;
	}

	@Override
	public PetAnimals getpetanimal(String animalsType) {
		if(animalsType == null)
		return null;
		 if(animalsType.equalsIgnoreCase("cat")){
	            return new Cat();
	        }else if(animalsType.equalsIgnoreCase("Dog")){
	            return new Dog();
	        }
	        return null;
	}

}
