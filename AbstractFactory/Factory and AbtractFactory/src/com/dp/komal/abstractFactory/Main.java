package com.dp.komal.abstractFactory;

public class Main {

	public static void main(String[] args) {
		  AbstractFactory petanimals = FactoryProduct.getFactory("Petanimals");
	        AbstractFactory wildanimals = FactoryProduct.getFactory("WildAnimals");

	       PetAnimals pa = petanimals.getpetanimal("cat");
	        pa.eat();
	        pa = petanimals.getpetanimal("dog");
	        pa.eat();

	       WildAnimals wa = wildanimals.getwildanimal("lion");
	       wa.eat();

	}

}
