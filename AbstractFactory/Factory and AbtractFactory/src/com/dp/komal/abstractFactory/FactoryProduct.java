package com.dp.komal.abstractFactory;

public class FactoryProduct {
	public static AbstractFactory getFactory(String factoryType){
        if(factoryType == null){
            return null;
        }
        if(factoryType.equalsIgnoreCase("petAnimals")){
            return new PetAnimalsFactory();
        }else
            if(factoryType.equalsIgnoreCase("wildAnimals")){
                return new WildAnimalsFactory();
            }

        return null;
    }

}
