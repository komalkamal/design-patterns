package com.dp.komal.abstractFactory;

public interface AbstractFactory {

	public abstract WildAnimals getwildanimal(String animalsType);
	public abstract PetAnimals getpetanimal(String animalsType);
	
	
}
