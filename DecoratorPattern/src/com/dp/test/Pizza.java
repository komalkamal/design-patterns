package com.dp.test;

public abstract class Pizza {
	abstract public int cost();
	abstract public String bill();

}
