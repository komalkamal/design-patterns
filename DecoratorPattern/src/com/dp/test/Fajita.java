package com.dp.test;

public class Fajita extends Topping
{

	public Fajita(Pizza pizza) {
		super(pizza);
		
		
		
	}
	public int cost()
	{
		return super.pizza.cost()+1500;
	}
	
	@Override
	public String bill() {
		
		return super.pizza.bill() + "\n" +  "Fajita = "+ 1500;
	}
	

}
