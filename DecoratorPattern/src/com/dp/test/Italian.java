package com.dp.test;

public class Italian extends Pizza{

	@Override
	public int cost() {
		
		return 500;
	}

	@Override
	public String bill() {
		
		return "Italian Dough = "+cost();
	}

}
