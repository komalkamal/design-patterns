package com.dp.test;

public class Local extends Pizza{

	@Override
	public int cost() {
	
		return 50;
	}

	@Override
	public String bill() {
	
		return "Local Dough = "+cost();
	}

}
