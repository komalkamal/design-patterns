package com.dp.test;

public class Vegetable extends Topping {

	public Vegetable(Pizza pizza) {
		super(pizza);
		
	}
	public int cost()
	{
		return super.pizza.cost()+800;
	}

	@Override
	public String bill() {
		
		return super.pizza.bill() + "\n " + "Vegetable = "+ 800;
	}
}
