package com.dp.test;

public class Supreme extends Topping{

	public Supreme(Pizza pizza) {
		super(pizza);
		
	}
	public int cost()
	{
		return super.pizza.cost()+1200;
	}
	
	@Override
	public String bill() {
		
		return super.pizza.bill() + "\n" + "Supreme ="+ 1200;
	}

}
