package com.dp.test;

public class PizzaFasad {
	
	public static Pizza getFajitaWithItalizanDough()
	{
		return new Fajita(new Italian());
	}

	public static Pizza getFajitaWithLocalDough()
	{
		return new Fajita(new Local());
		
	}
	
	public static Pizza getSupremeWithItalianDough()
	{
		return new Supreme(new Italian());	}
}
