package com.dependencyinversion.Lifeprocess;



import com.dependencyinversion.interfaces.ILivingThing;

public class Human implements ILivingThing {

	@Override
	public void move() {
		
		System.out.println("Human are living_thing");
	}

}
