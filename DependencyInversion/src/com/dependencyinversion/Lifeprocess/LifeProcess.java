package com.dependencyinversion.Lifeprocess;

import com.dependencyinversion.interfaces.ILivingThing;
import com.dependencyinversion.interfaces.INonLivingThing;

public class LifeProcess {
	public void move(ILivingThing L)
	{
		L.move();
		
	}
	public void notMove(INonLivingThing N)
	{
		N.notMove();
	}

}
