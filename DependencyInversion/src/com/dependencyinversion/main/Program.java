package com.dependencyinversion.main;

import com.dependencyinversion.Lifeprocess.Furniture;
import com.dependencyinversion.Lifeprocess.Human;
import com.dependencyinversion.Lifeprocess.LifeProcess;

public class Program {

	public static void main(String[] args) {
		LifeProcess lp = new LifeProcess();
		
		Human h = new Human();
		
		 lp.move(h);
		
		Furniture f = new Furniture();
		lp.notMove(f);
		
		

	}

}
